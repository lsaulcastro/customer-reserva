package reserva.shared.db.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import reserva.shared.db.entity.base.BaseEntity;

import java.io.Serializable;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Entity(name = "PHONE")
public class PhoneEntity extends BaseEntity implements Serializable {


    /**
     *
     */
    @Column(name = "AREA_CODE")
    private String areaCode;

    /**
     *
     */
    @Column(name = "NUMBER")
    private String number;

    /**
     *
     */
    @Column(name = "TYPE")
    private String type;


    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
