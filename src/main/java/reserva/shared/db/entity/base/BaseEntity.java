package reserva.shared.db.entity.base;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;

import java.time.LocalDateTime;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Embeddable
@MappedSuperclass
public abstract class BaseEntity extends PanacheEntityBase {


    /**
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private Integer id;

    /**
     *
     */
    @Column(name = "CREATED", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime created;

    /**
     *
     */
    @Column(name = "UPDATED")
    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime updated;

    /**
     *
     */
    @Column(name = "ACTIVE")
    private Boolean active;

    @PrePersist
    private void prePersist(){
        this.setCreated(LocalDateTime.now());
        this.setUpdated(LocalDateTime.now());
        this.setActive(Boolean.TRUE);
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
