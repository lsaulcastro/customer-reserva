package reserva.shared.db.entity;

import jakarta.persistence.*;
import reserva.shared.db.entity.base.BaseEntity;

import java.io.Serializable;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Entity(name = "CUSTOMER")
public class CustomerEntity extends BaseEntity implements Serializable {

    /**
     *
     */
    @Column(name = "FIRST_NAME", nullable = false)
    private String firstName;

    /**
     *
     */
    @Column(name = "SECOND_NAME")
    private String secondName;

    /**
     *
     */
    @Column(name = "LAST_NAME", nullable = false)
    private String lastName;

    /**
     *
     */
    @Column(name = "SECOND_SURNAME")
    private String secondSurname;

    /**
     *
     */
    @Column(name = "EMAIL",nullable = false)
    private String email;

    /**
     *
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "ID", nullable = false)
    private AddressEntity address;

    /**
     *
     */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PHONE_ID", referencedColumnName = "ID", nullable = false)
    private PhoneEntity phone;

    /**
     *
     */
    @Column(name = "GENTILITY", nullable = false)
    private String gentility;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity address) {
        this.address = address;
    }

    public PhoneEntity getPhone() {
        return phone;
    }

    public void setPhone(PhoneEntity phone) {
        this.phone = phone;
    }

    public String getGentility() {
        return gentility;
    }

    public void setGentility(String gentility) {
        this.gentility = gentility;
    }
}
