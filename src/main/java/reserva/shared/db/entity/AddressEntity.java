package reserva.shared.db.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import reserva.shared.db.entity.base.BaseEntity;

import java.io.Serializable;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Entity(name = "ADDRESS")
public class AddressEntity extends BaseEntity implements Serializable {

    /**
     *
     */
    @Column(name = "STREET")
    private String street;

    /**
     *
     */
    @Column(name = "STATE")
    private String state;

    /**
     *
     */

    @Column(name = "COUNTRY", nullable = false)
    private String country;

    /**
     *
     */
    @Column(name = "CITY")
    private String city;

    /**
     *
     */
    @Column(name = "LINE_ONE")
    private String line1;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }
}
