package reserva.controllers.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public class ResponseGenericDto<T> {

    @JsonProperty("statusCode")
    private Integer status;

    @JsonProperty("data")
    private T response;

    @JsonProperty("error")
    private List<String> errors;

    public ResponseGenericDto(Integer status, T response) {
        this.status = status;
        this.response = response;
    }

    public ResponseGenericDto(Integer status, List<String> errors) {
        this.status = status;
        this.errors = errors;
    }
}
