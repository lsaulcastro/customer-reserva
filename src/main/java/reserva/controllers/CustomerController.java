package reserva.controllers;

import jakarta.inject.Inject;
import jakarta.validation.Valid;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import reserva.controllers.dto.ResponseGenericDto;
import reserva.models.services.customer.ICustomerServiceInterface;
import reserva.models.services.dto.CustomerDto;
import reserva.models.services.dto.CustomerSaveDto;

import java.util.List;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Path("api/customer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerController {

    @Inject
    ICustomerServiceInterface iCustomerService;

    public CustomerController(ICustomerServiceInterface iCustomerService) {
        this.iCustomerService = iCustomerService;
    }

    @GET
    @Path("/list")
    public ResponseGenericDto<List<CustomerDto>> getAllCustomer() {
        return iCustomerService.getAllCustomer();
    }

    @GET
    @Path("/country")
    public ResponseGenericDto<List<CustomerDto>> getByCountry(@QueryParam("country") String country) {
        return iCustomerService.getByCountry(country);
    }

    @GET
    @Path("{id}")
    public ResponseGenericDto<CustomerDto> getById(@PathParam("id") Integer id) {
        return iCustomerService.getById(id);
    }

    @POST
    public ResponseGenericDto<CustomerDto> createCustomer(@Valid CustomerDto customerDto) {
        return iCustomerService.saveCustomer(customerDto);
    }

    @PUT
    @Path("{id}")
    public ResponseGenericDto<CustomerDto> updateCustomer(@PathParam("id") Integer id, @Valid CustomerSaveDto customerSaveDto) {
        return iCustomerService.updateCustomer(id, customerSaveDto);
    }

    @DELETE
    @Path("{id}")
    public ResponseGenericDto<Boolean> deleteCustomer(@PathParam("id") Integer id) {
        return iCustomerService.deleteById(id);
    }
}
