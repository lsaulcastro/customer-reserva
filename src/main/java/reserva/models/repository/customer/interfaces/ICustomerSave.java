package reserva.models.repository.customer.interfaces;

import reserva.shared.db.entity.CustomerEntity;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public interface ICustomerSave {

    CustomerEntity saveEntity(CustomerEntity entity);
}
