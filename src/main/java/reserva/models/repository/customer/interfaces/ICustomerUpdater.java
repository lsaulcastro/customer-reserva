package reserva.models.repository.customer.interfaces;

import reserva.models.services.dto.CustomerSaveDto;
import reserva.shared.db.entity.CustomerEntity;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public interface ICustomerUpdater {

    CustomerEntity updateEntity(Integer entity, CustomerSaveDto customerDto);

}
