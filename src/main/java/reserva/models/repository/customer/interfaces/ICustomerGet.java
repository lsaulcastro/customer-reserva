package reserva.models.repository.customer.interfaces;

import reserva.shared.db.entity.CustomerEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */

public interface ICustomerGet {

    Optional<CustomerEntity> getById(Integer id);

    List<CustomerEntity> getByCountry(String country);

    List<CustomerEntity> getAllCustomer();
}
