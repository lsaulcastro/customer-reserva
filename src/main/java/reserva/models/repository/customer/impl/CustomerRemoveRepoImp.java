package reserva.models.repository.customer.impl;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import reserva.models.repository.customer.interfaces.ICustomerRemove;
import reserva.shared.db.entity.CustomerEntity;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Singleton
public class CustomerRemoveRepoImp implements ICustomerRemove, PanacheRepositoryBase<CustomerEntity, Integer> {

    /**
     *
     */
    @Inject
    CustomerGetRepositoryImpl customerRepoGet;


    @Override
    public Boolean removeCustomer(Integer id) {
        CustomerEntity entity = customerRepoGet.findById(id);
        entity.setActive(Boolean.FALSE);
        entity.getAddress().setActive(Boolean.FALSE);
        entity.getAddress().setActive(Boolean.FALSE);
        entity.persist();
        return entity.isPersistent();
    }
}
