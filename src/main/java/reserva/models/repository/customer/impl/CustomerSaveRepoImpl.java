package reserva.models.repository.customer.impl;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.NotFoundException;
import reserva.models.repository.customer.interfaces.ICustomerSave;
import reserva.shared.db.entity.CustomerEntity;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Singleton
@Transactional
public class CustomerSaveRepoImpl implements ICustomerSave, PanacheRepositoryBase<CustomerEntity, Integer> {

    /**
     *
     */
    private final Logger logger = Logger.getLogger(CustomerSaveRepoImpl.class.getName());

    @Override
    public CustomerEntity saveEntity(CustomerEntity entity) {

        try {
            entity.getAddress().persist();
            entity.getPhone().persist();
            if (entity.getAddress().isPersistent() && entity.getPhone().isPersistent()) entity.persistAndFlush();

        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage());
        }

        if (!entity.isPersistent() || Objects.isNull(entity.getId()))
            throw new NotFoundException("No se guardo el cliente");

        return this.findById(entity.getId());
    }
}
