package reserva.models.repository.customer.impl;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.inject.Singleton;
import reserva.models.repository.customer.interfaces.ICustomerGet;
import reserva.shared.db.entity.CustomerEntity;

import java.util.List;
import java.util.Optional;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Singleton
public class CustomerGetRepositoryImpl implements ICustomerGet, PanacheRepositoryBase<CustomerEntity, Integer> {


    @Override
    public Optional<CustomerEntity> getById(Integer id) {
        return Optional.ofNullable(find("FROM CUSTOMER WHERE ID = ?1 AND active = true", id).firstResult());
    }

    @Override
    public List<CustomerEntity> getByCountry(String country) {
        return  find("FROM CUSTOMER c JOIN ADDRESS a ON c.address.id = a.id WHERE a.country = ?1 AND c.active = true", country.toUpperCase()).list();
    }

    @Override
    public List<CustomerEntity> getAllCustomer() {
        return listAll();
    }
}
