package reserva.models.repository.customer.impl;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.transaction.Transactional;
import reserva.models.mappers.ICustomerMapper;
import reserva.models.repository.customer.interfaces.ICustomerUpdater;
import reserva.models.services.dto.CustomerSaveDto;
import reserva.shared.db.entity.CustomerEntity;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Singleton
@Transactional
public class CustomerUpdateRepoImpl implements ICustomerUpdater, PanacheRepositoryBase<CustomerEntity, Integer> {


    /**
     *
     */
    @Inject
    CustomerSaveRepoImpl saveRepo;

    /**
     *
     */
    @Inject
    CustomerGetRepositoryImpl customerGet; ;

    /**
     *
     */
    @Inject
    ICustomerMapper mapper;


    @Override
    public CustomerEntity updateEntity(Integer id, CustomerSaveDto customerDto) {
        CustomerEntity currentEntity = customerGet.findById(id);
        CustomerEntity sourceEntity = mapper.mapToEntitySave(customerDto);

        if (Objects.nonNull(sourceEntity.getAddress()))
            currentEntity.setAddress(sourceEntity.getAddress());

        if (Objects.nonNull(sourceEntity.getPhone()))
            currentEntity.setPhone(sourceEntity.getPhone());

        currentEntity.setEmail(sourceEntity.getEmail());
        currentEntity.setUpdated(LocalDateTime.now());

        return saveRepo.saveEntity(currentEntity);
    }
}
