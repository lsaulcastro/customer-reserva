package reserva.models.services.customer;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import reserva.controllers.dto.ResponseGenericDto;
import reserva.models.mappers.ICustomerMapper;
import reserva.models.repository.customer.interfaces.ICustomerGet;
import reserva.models.repository.customer.interfaces.ICustomerRemove;
import reserva.models.repository.customer.interfaces.ICustomerSave;
import reserva.models.repository.customer.interfaces.ICustomerUpdater;
import reserva.models.services.dto.CustomerDto;
import reserva.models.services.dto.CustomerSaveDto;
import reserva.shared.db.entity.CustomerEntity;

import java.util.List;
import java.util.Objects;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Singleton
public class CustomerServiceImpl implements ICustomerServiceInterface {

    /**
     *
     */
    @Inject
    ICustomerGet iCustomerGet;

    /**
     *
     */
    @Inject
    ICustomerMapper iCustomerMapper;

    /**
     *
     */
    @Inject
    ICustomerSave iCustomerSave;

    /**
     *
     */
    @Inject
    ICustomerUpdater iCustomerUpdater;

    /**
     *
     */
    @Inject
    ICustomerRemove iCustomerRemove;

    @Override
    public ResponseGenericDto<CustomerDto> getById(Integer id) {
        return new ResponseGenericDto<>(Response.ok().build().getStatus(),
                iCustomerMapper.mapToDto(this.getEntityById(id)));
    }

    @Override
    public ResponseGenericDto<List<CustomerDto>> getByCountry(String country) {
        return new ResponseGenericDto<>(Response.ok().build().getStatus(),
                iCustomerMapper.mapToDtoList(iCustomerGet.getByCountry(country)));
    }

    @Override
    public ResponseGenericDto<List<CustomerDto>> getAllCustomer() {
        return new ResponseGenericDto<>(Response.ok().build().getStatus(),
                iCustomerMapper.mapToDtoList(iCustomerGet.getAllCustomer()));
    }

    @Override
    public ResponseGenericDto<CustomerDto> saveCustomer(CustomerDto customerDto) {
        CustomerEntity entity = iCustomerMapper.mapToEntity(customerDto);
        entity = iCustomerSave.saveEntity(entity);
        return new ResponseGenericDto<>(Response.created(null).build().getStatus(),
                iCustomerMapper.mapToDto(entity));
    }

    @Override
    public ResponseGenericDto<CustomerDto> updateCustomer(Integer id, CustomerSaveDto customerDto) {
        return new ResponseGenericDto<>(Response.accepted().build().getStatus(),
                iCustomerMapper.mapToDto(iCustomerUpdater.updateEntity(id, customerDto)));
    }

    @Override
    public ResponseGenericDto<Boolean> deleteById(Integer id) {
        return new ResponseGenericDto<>(Response.ok().build().getStatus(),
                iCustomerRemove.removeCustomer(id));
    }

    private CustomerEntity getEntityById(Integer id) {
        return iCustomerGet.getById(id)
                .orElseThrow(() -> new NotFoundException("No se encontro el cliente especificado."));
    }

}
