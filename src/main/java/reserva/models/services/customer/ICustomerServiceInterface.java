package reserva.models.services.customer;

import reserva.controllers.dto.ResponseGenericDto;
import reserva.models.services.dto.CustomerDto;
import reserva.models.services.dto.CustomerSaveDto;

import java.util.List;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public interface ICustomerServiceInterface {

    ResponseGenericDto<CustomerDto> getById(Integer id);

    ResponseGenericDto<List<CustomerDto>> getByCountry(String country);

    ResponseGenericDto<List<CustomerDto>> getAllCustomer();

    ResponseGenericDto<CustomerDto> saveCustomer(CustomerDto customerDto);
    ResponseGenericDto<CustomerDto> updateCustomer(Integer id, CustomerSaveDto customerSaveDto);

    ResponseGenericDto<Boolean> deleteById(Integer id);

}
