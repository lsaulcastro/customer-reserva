package reserva.models.services.dto;

import jakarta.validation.constraints.NotBlank;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public class PhoneGetDto {


    /**
     *
     */
    @NotBlank(message = "areaCode required")
    private String areaCode;

    /**
     *
     */
    @NotBlank(message = "number required")
    private String number;

    /**
     *
     */
    private String type;

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
