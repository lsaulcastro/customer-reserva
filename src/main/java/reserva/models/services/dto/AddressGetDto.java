package reserva.models.services.dto;

import jakarta.validation.constraints.NotBlank;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public class AddressGetDto {


    /**
     *
     */
    private String street;

    /**
     *
     */
    private String state;

    /**
     *
     */
    @NotBlank(message = "address.country required")
    private String country;

    /**
     *
     */
    private String city;

    /**
     *
     */
    private String line1;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }
}
