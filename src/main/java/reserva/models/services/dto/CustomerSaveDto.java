package reserva.models.services.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public class CustomerSaveDto {


    /**
     *
     */
    @Email(message = "field must be email")
    @NotBlank(message = "email required")
    private String email;

    /**
     *
     */
    @NotBlank(message = "address required")
    private AddressGetDto address;

    /**
     *
     */
    @NotBlank(message = "phone required")
    private PhoneGetDto phone;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressGetDto getAddress() {
        return address;
    }

    public void setAddress(AddressGetDto address) {
        this.address = address;
    }

    public PhoneGetDto getPhone() {
        return phone;
    }

    public void setPhone(PhoneGetDto phone) {
        this.phone = phone;
    }

}
