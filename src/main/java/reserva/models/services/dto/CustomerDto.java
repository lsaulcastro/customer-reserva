package reserva.models.services.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
public class CustomerDto {


    /**
     *
     */
    @NotBlank(message = "firstName required")
    private String firstName;

    /**
     *
     */
    private String secondName;

    /**
     *
     */
    @NotBlank(message = "lastName required")
    private String lastName;

    /**
     *
     */
    private String secondSurname;

    /**
     *
     */
    @NotBlank(message = "email required")
    @Email(message = "field must be email")
    private String email;

    /**
     *
     */

    @NotNull(message = "address required")
    private AddressGetDto address;

    /**
     *
     */
    @NotNull(message = "phone required")
    private PhoneGetDto phone;

    /**
     *
     */
    private String gentility;
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AddressGetDto getAddress() {
        return address;
    }

    public void setAddress(AddressGetDto address) {
        this.address = address;
    }

    public PhoneGetDto getPhone() {
        return phone;
    }

    public void setPhone(PhoneGetDto phone) {
        this.phone = phone;
    }

    public String getGentility() {
        return gentility;
    }

    public void setGentility(String gentility) {
        this.gentility = gentility;
    }
}
