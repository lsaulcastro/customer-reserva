package reserva.models.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import reserva.models.services.dto.CustomerDto;
import reserva.models.services.dto.CustomerSaveDto;
import reserva.shared.db.entity.CustomerEntity;

import java.util.List;

/**
 * @author Saul Castro
 * @project user.crud
 * @since 2/15/2024
 */
@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ICustomerMapper {

    /**
     *
     */
    CustomerDto mapToDto(CustomerEntity customer);

    /**
     *
     */
    @Mapping(target = "id", ignore = true)
    CustomerEntity mapToEntity(CustomerDto dto);

    /**
     *
     */
    List<CustomerDto> mapToDtoList(List<CustomerEntity> customer);

    /**
     *
     */
    @Mapping(target = "id", ignore = true)
    CustomerEntity mapToEntitySave(CustomerSaveDto dto);

}
